<?php

namespace api\modules\v1\controllers;

use api\modules\v1\models\User;
use common\models\queueJobs\CreateUserJob;
use Yii;
use yii\rest\ActiveController;

/**
 * User Controller API
 */
class UserController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\User';


    public function actions()
    {
        $actions = parent::actions();
        unset($actions['create']);
        return $actions;
    }

    /**
     * Add task to RabbitMQ queue or return validate errors
     * @return User
     */
    public function actionCreate()
    {
        $model = new User();
        $model->load(Yii::$app->request->post(), '');
        if ($model->validate()) {
            Yii::$app->queue->push(new CreateUserJob([
                'name' => $model->name,
                'email' => $model->email,
                'age' => $model->age,
                'gender' => $model->gender,
            ]));
            $response = Yii::$app->response;
            $response->setStatusCode(202);
        }
        return $model;

    }
}


