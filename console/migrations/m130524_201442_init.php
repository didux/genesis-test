<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{

    /**
     * migration table name
     */
    public $tableName = '{{%user}}';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(

            ),
            'name' => $this->string()->notNull()->comment('User name'),
            'email' => $this->string()->notNull()->unique()->comment('Email'),
            'age' => $this->integer()->defaultValue(null)->comment('Age'),
            'gender' => $this->integer()->defaultValue(null)->comment('Gender'),

            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable($this->tableName);
    }
}
