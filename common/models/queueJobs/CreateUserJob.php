<?php
/**
 * @author Igor Medved
 */

namespace common\models\queueJobs;

use common\models\User;
use yii\base\Object;


class CreateUserJob extends Object implements \yii\queue\JobInterface
{
    public $name;
    public $email;
    public $age;
    public $gender;

    public function execute($queue)
    {
        $user = new User();
        $user->name = $this->name;
        $user->email = $this->email;
        $user->age = $this->age;
        $user->gender = $this->gender;
        $user->save(false);
    }
}