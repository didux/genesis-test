<?php
namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * User model
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property integer $age
 * @property integer $gender
 * @property integer $created_at
 * @property integer $updated_at
 */
class User extends ActiveRecord
{
    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email'], 'required'],
            [['name', 'email'], 'string', 'max' => 255],
            ['email', 'email'],
            [['age', 'gender'], 'integer'],
            ['gender', 'validateGender'],
        ];
    }

    /**
     * @return array
     */
    public static function getGender()
    {
        return [
            self::GENDER_MALE => 'Male',
            self::GENDER_FEMALE => 'Female',
        ];
    }

    public function validateGender($attribute)
    {
        if (!in_array($this->gender, array_keys(self::getGender()))) {
            $msg = 'Please specify ' . self::GENDER_MALE . ' for ' . self::getGender()[self::GENDER_MALE] .
                ', or ' . self::GENDER_FEMALE . ' for ' . self::getGender()[self::GENDER_FEMALE];
            $this->addError($attribute, $msg);
        }
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['name' => $username]);
    }


    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

}
